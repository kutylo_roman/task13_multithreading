package com.kutylo.model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Monitor {
    private int firstEntry = 1;
    private int secondEntry = 2;
    private int thirdEntry = 3;

    public int getFirstEntry() {
        return firstEntry;
    }

    public void setFirstEntry(int firstEntry) {
        this.firstEntry = firstEntry;
    }

    List<Integer> array = new LinkedList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20));

    public List<Integer> getArray() {
        return array;
    }

    public void addToArray(Integer value){
         array.add(value);
    }
}
